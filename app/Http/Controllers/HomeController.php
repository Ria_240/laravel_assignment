<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends BaseController
{
    public function index()
	{
		
		return view('homepage');
	
	}
	public function about()
	{
		
		return view('about');
	
	}
	
	    public function store(Request $request)
		{
			 $firstname = $request->input('first_name');
			 $lastname = $request->input('last_name');
			 $address = $request->input('address');
			 $email = $request->input('email');
			 $data=array('first_name'=>$firstname,"last_name"=>$lastname,"address"=>$address,"email"=>$email);
			 DB::table('first_laravel')->insert($data);
			 echo "Record inserted successfully.<br/>";
			 //echo "<pre>"; print_r($request->all());
		}
	
		public function view_records() 
		{
			$users = DB::select('select * from first_laravel');
			return view('view-records',['users'=>$users]);	
		}
}