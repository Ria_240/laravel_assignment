<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AdminloginController extends Controller
{
    public function index()
	{
		return view('admin.login');
	}
	public function dashboard()
	{ 
		return view('admin.dashboard');
	}
	public function logout()
	{ 
		return redirect('admin/login');
	}
}