<!DOCTYPE html>
<html lang="en">
<head>
  <title>home page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700&amp;subset=latin-ext" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ URL::asset('css/custum-style.css') }}">
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>  
</head>
<body>
 <header class="top-header">
        <div class="top-bar">
		    <div class="container">
			   <ul class="site-info">
			      <li><a href="mailto:support@elissa-psychic.com"><i class="fas fa-envelope-open"></i>support@elissa-psychic.com</a></li>
				<li><a href="tel:(234) 301-3513"><i class="fas fa-phone"></i>(234) 301-3513</a></li>
                <li class="scl-icn"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			    <li class="scl-icn"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			    <li class="scl-icn"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
               <li class="scl-icn"><a href="#"><i class="fab fa-google"></i></a></li>				
			   </ul>			    
			</div>
		</div>
	     <nav class="navbar navbar-expand-md navbar-light hdr-main">
		 <div class="container">
		  <div class="logo-img"><a class="navbar-brand" href="#"><img src="{{ URL::asset('images/logo-new.png') }}"></a></div>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav">
			  <li class="nav-item">
				<a class="nav-link active" href="#">Home</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="#">Who Is Elissa? </a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="#">Blog of the Angels</a>
			  </li>		 
			  <li class="nav-item">
				<a class="nav-link" href="#">Angel Boutique</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="#">Cart</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="#">Contact us</a>
			  </li> 	  
			</ul>
		</div>
        </div>		  
		</nav>
    </header>
</body>
</html>	
	